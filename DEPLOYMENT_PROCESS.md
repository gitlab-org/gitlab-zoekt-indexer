# Deployment process

Zoekt deployment involves two main parts: `gitlab-zoekt-indexer` (this project) and `gitlab-zoekt-webserver`. 
This document describes the process for deploying each of those parts and involves multiple projects.

_Note:_ `gitlab-zoekt-dynamic-indexer` is no longer used. It is not included in these steps

## gitlab-zoekt-indexer

1. Pick the commit SHA from [gitlab-zoekt-indexer](https://gitlab.com/gitlab-org/gitlab-zoekt-indexer/-/commits/main?ref_type=heads)
1. Add a new CI Docker image for Zoekt
   - Use the following naming convention and bump the version: `Dockerfile.zoekt-ci-image-X.Y`. 
   - Update the `GITLAB_ZOEKT_INDEXER_VERSION` to the new SHA
   - You may need to update the Go and/or Alpine versions. The Go version should match the [version specified in CNG](https://gitlab.com/gitlab-org/build/CNG/-/blob/0c2cb9de0bf117b012b1539765c2fba7c7298717/ci_files/variables.yml#L31).
1. Update the [Zoekt image](https://gitlab.com/gitlab-org/gitlab/-/blob/12db2f6d505b1adca1f309537187e3cb38d4e730/.gitlab/ci/global.gitlab-ci.yml#L316) used in GitLab CI
1. Update the CNG Docker image
   - Set `ZOEKT_INDEXER_VERSION` to the new SHA in [gitlab-zoekt-indexer/Dockerfile](https://gitlab.com/gitlab-org/build/CNG/-/blob/b1ef63a1e16149b881d93cd7ed3e486f82706735/gitlab-zoekt-indexer/Dockerfile#L11)
   - Set `GITLAB_ZOEKT_INDEXER_VERSION` to a new version following the format `v.X.Y.Z-SHA` in [`ci_files/variables.yml`](https://gitlab.com/gitlab-org/build/CNG/-/blob/ffc80ba1044f5a1a3a7d71f568b6a319472c10f7/ci_files/variables.yml#L21)
   - Set `ZOEKT_INDEXER_VERSION` to the new SHA in [`ci_files/variables.yml`](https://gitlab.com/gitlab-org/build/CNG/-/blob/ffc80ba1044f5a1a3a7d71f568b6a319472c10f7/ci_files/variables.yml#L23)
1. Update the [gitlab-zoekt chart](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt)
   - Update the `indexer` `tag` field to the value from `GITLAB_ZOEKT_INDEXER_VERSION` in [`values.yaml`](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/blob/b542e19f4b26cf8272553a1f2e2d841742f44d30/values.yaml#L29)
   - Increase the chart `version` in [`Chart.yaml`](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/blob/b542e19f4b26cf8272553a1f2e2d841742f44d30/Chart.yaml#L5)
1. Create a [new tag](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt) matching the chart `version`
1. Update the [gitlab chart](https://gitlab.com/gitlab-org/charts/gitlab)
   - Update the chart `version` in [`requirements.yaml`](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/c70ee02cd9e7f2c623b4e2aecafacabfd0d32daf/requirements.yaml#L39)

## zoekt webserver

1. Add a new CI Docker image for Zoekt
   - Use the following naming convention and bump the version: `Dockerfile.zoekt-ci-image-X.Y`. 
   - Update the `ZOEKT_VERSION` to the new SHA
   - You may need to update the Go and/or Alpine versions. The Go version should match what [version specified in CNG](https://gitlab.com/gitlab-org/build/CNG/-/blob/0c2cb9de0bf117b012b1539765c2fba7c7298717/ci_files/variables.yml#L31).
1. Update the [Zoekt image](https://gitlab.com/gitlab-org/gitlab/-/blob/12db2f6d505b1adca1f309537187e3cb38d4e730/.gitlab/ci/global.gitlab-ci.yml#L316) used in GitLab CI
1. Update the CNG Docker image
   - Set `ZOEKT_VERSION` to the updated SHA in [gitlab-zoekt-webserver/Dockerfile](https://gitlab.com/gitlab-org/build/CNG/-/blob/b1ef63a1e16149b881d93cd7ed3e486f82706735/gitlab-zoekt-webserver/Dockerfile#L11)
   - Set `GITLAB_ZOEKT_WEBSERVER_VERSION` to a new version following the format `v.X.Y.Z-SHA` in [`ci_files/variables.yml`](https://gitlab.com/gitlab-org/build/CNG/-/blob/ffc80ba1044f5a1a3a7d71f568b6a319472c10f7/ci_files/variables.yml#L19)
   - Set `ZOEKT_VERSION` to the new SHA in [`ci_files/variables.yml`](https://gitlab.com/gitlab-org/build/CNG/-/blob/ffc80ba1044f5a1a3a7d71f568b6a319472c10f7/ci_files/variables.yml#L22)
1. Update the [gitlab-zoekt chart](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt)
   - Update the `webserver` `tag` field to the value from `GITLAB_ZOEKT_WEBSERVER_VERSION` in [`values.yaml`](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/blob/b542e19f4b26cf8272553a1f2e2d841742f44d30/values.yaml#L36)
   - Increase the chart `version` in [`Chart.yaml`](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/blob/b542e19f4b26cf8272553a1f2e2d841742f44d30/Chart.yaml#L5)
1. Create a [new tag](https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt) matching the chart `version`
1. Update the [gitlab chart](https://gitlab.com/gitlab-org/charts/gitlab)
   - Update the chart `version` in [`requirements.yaml`](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/c70ee02cd9e7f2c623b4e2aecafacabfd0d32daf/requirements.yaml#L39)
