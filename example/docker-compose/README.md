### Instructions on how to set up zoekt with docker compose

1. Copy the `example.env` file
    ```
    cp example.env .env
    ```
1. Start zoekt with docker-compose. Please specify the UID/GID of the account that has access to gitaly socket file (usually it's `git`). You can also specify it in the `docker-compose.yml` file inline.
    ```
    UID=1000 GID=1000 docker-compose up
    ```