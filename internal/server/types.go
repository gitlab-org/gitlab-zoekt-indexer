package server

import (
	"context"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/search"
)

type DefaultIndexBuilder struct {
	IndexDir string
}

type DeleteRequest struct {
	RepoID   uint32                   `json:"RepoID"`
	Callback *callback.CallbackParams `json:"Callback"`
}

type GitalyConnectionInfo struct {
	Address string `json:"Address"`
	Token   string `json:"Token"`
	Storage string `json:"Storage"`
	Path    string `json:"Path"`
}

type IndexRequest struct {
	Timeout              string                   `json:"Timeout"`
	RepoID               uint32                   `json:"RepoID"`
	GitalyConnectionInfo *GitalyConnectionInfo    `json:"GitalyConnectionInfo"`
	FileSizeLimit        int                      `json:"FileSizeLimit"`
	Callback             *callback.CallbackParams `json:"Callback"`
	Force                bool                     `json:"Force"`
	OptimizedPerformance bool                     `json:"OptimizedPerformance"`
}

type IndexServer struct {
	PathPrefix           string
	IndexBuilder         indexBuilder
	CallbackApi          callback.CallbackApi
	promRegistry         *prometheus.Registry
	metricsRequestsTotal *prometheus.CounterVec
	IndexingLock         *indexing_lock.IndexingLock
	Searcher             *search.Searcher
}

type indexBuilder interface {
	DeleteRepository(DeleteRequest, callback.CallbackFunc, *indexing_lock.IndexingLock) error
	GetIndexDir() string
	IndexRepository(context.Context, IndexRequest, callback.CallbackFunc) error
}
