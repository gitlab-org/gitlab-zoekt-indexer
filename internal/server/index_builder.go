package server

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/file_cleaner"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexer"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/labkit/correlation"
)

func (b DefaultIndexBuilder) DeleteRepository(req DeleteRequest, callbackFunc callback.CallbackFunc, lock *indexing_lock.IndexingLock) error {
	fileCleaner := file_cleaner.NewFileCleaner(b.GetIndexDir(), lock)

	if err := fileCleaner.MarkRepoForDeletion(req.RepoID); err != nil {
		if req.Callback != nil {
			callbackFunc.OnFailure(*req.Callback, err)
		}
		return err
	}

	callbackFunc.OnSuccess(*req.Callback)

	return nil
}

func (b DefaultIndexBuilder) GetIndexDir() string {
	return b.IndexDir
}

func (b DefaultIndexBuilder) IndexRepository(ctx context.Context, req IndexRequest, callbackFunc callback.CallbackFunc) error {
	if req.GitalyConnectionInfo == nil {
		return errors.New("gitalyConnectionInfo is not set")
	}

	if req.FileSizeLimit == 0 {
		return errors.New("fileSizeLimit is not set")
	}

	timeout, err := time.ParseDuration(req.Timeout)
	if err != nil {
		return fmt.Errorf("failed to parse Timeout: %v with error %w", req.Timeout, err)
	}

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	idx := &indexer.Indexer{
		IndexDir:             b.IndexDir,
		ProjectID:            req.RepoID,
		GitalyAddress:        req.GitalyConnectionInfo.Address,
		GitalyStorageName:    req.GitalyConnectionInfo.Storage,
		GitalyToken:          req.GitalyConnectionInfo.Token,
		GitalyRelativePath:   req.GitalyConnectionInfo.Path,
		LimitFileSize:        req.FileSizeLimit,
		ForceReindex:         req.Force,
		Writer:               &indexer.DefaultIndexWriter{},
		OnRetryableFailure:   indexer.IndexingFailureFallback,
		OptimizedPerformance: req.OptimizedPerformance,
	}

	ctx = correlation.ContextWithCorrelation(ctx, correlation.ExtractFromContextOrGenerate(ctx))
	indexingResult, err := idx.IndexRepository(ctx)
	if indexingResult != nil {
		slog.Debug("indexing Result", "project_id", idx.ProjectID, "force", idx.ForceReindex, "modified_files_count", indexingResult.ModifiedFilesCount, "deleted_files_count", indexingResult.DeletedFilesCount)
	}

	railsCallback := req.Callback

	if err != nil {
		if railsCallback != nil {
			callbackFunc.OnFailure(*railsCallback, err)
		}
		return err
	}

	if railsCallback != nil {
		callbackFunc.OnSuccess(*railsCallback)
	}

	return nil
}
