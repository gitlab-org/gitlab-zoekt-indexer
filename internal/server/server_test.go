package server

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/sourcegraph/zoekt"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/debug_ls"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/file_cleaner"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/projectpath"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/search"
)

const (
	pathPrefix = "/indexer"
	nodeUUID   = "3869fe21-36d1"
)

type indexBuilderMock struct {
	IndexDir           string
	doneChannel        chan interface{}
	startChannel       chan interface{}
	repositoryResponse error
	block              bool
}

func (b indexBuilderMock) IndexRepository(ctx context.Context, req IndexRequest, callbackFunc callback.CallbackFunc) error {
	if b.block {
		close(b.startChannel)

		<-b.doneChannel
	}

	return b.repositoryResponse
}

func (b indexBuilderMock) DeleteRepository(req DeleteRequest, callbackFunc callback.CallbackFunc, lock *indexing_lock.IndexingLock) error {
	if b.block {
		close(b.startChannel)

		<-b.doneChannel
	}

	return b.repositoryResponse
}

func (b indexBuilderMock) GetIndexDir() string {
	return b.IndexDir
}

func defaultIndexServer() IndexServer {
	return IndexServer{
		PathPrefix:   pathPrefix,
		IndexingLock: indexing_lock.NewIndexingLock(),
		Searcher: search.NewSearcher(&http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}),
		CallbackApi: callback.CallbackApi{
			Client: &http.Client{},
		},
	}
}

func createTestFiles(dir string, files []string) error {
	errs := make([]error, 0, len(files))

	for _, file := range files {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		errs = append(errs, err)
	}

	return errors.Join(errs...)
}

func filterExistingFiles(dir string, files []string) []string {
	var existingFiles []string

	for _, file := range files {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	return existingFiles
}

func TestServerCreateIndexDir(t *testing.T) {
	dir := filepath.Join(t.TempDir(), "server_create_new_dir")

	s := IndexServer{
		IndexBuilder: indexBuilderMock{
			IndexDir: dir,
		},
	}

	s.createIndexDir()

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		t.Fatal("Directory does not exist")
	}
}

func TestCreateIndexDir(t *testing.T) {
	dir := filepath.Join(t.TempDir(), "create_new_dir")

	err := CreateIndexDir(dir)
	require.NoError(t, err, "Error creating directory")

	_, err = os.Stat(dir)
	require.False(t, os.IsNotExist(err), "Directory does not exist")
}

func TestInitMetrics(t *testing.T) {
	server := defaultIndexServer()

	server.initMetrics()

	require.NotNil(t, server.promRegistry)

	require.NotNil(t, server.metricsRequestsTotal)
}

func TestHandleHealthCheck(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/health", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleMetrics(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/metrics", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.initMetrics()
	server.incrementRequestsTotal("GET", "/foo_bar", 200)
	server.handleMetrics()(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	require.Equal(t, http.StatusOK, resp.StatusCode)
	require.Regexp(t, "gitlab_zoekt_indexer_requests_total.*foo_bar", string(body))
}

func TestHandleStatus(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/index/10", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	type statusResponse struct {
		SHA     string `json:"sha"`
		Success bool   `json:"success"`
	}

	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	var parsedResponse statusResponse
	err := dec.Decode(&parsedResponse)

	require.NoError(t, err)

	expectedResponse := statusResponse{
		SHA:     "5f6ffd6461ba03e257d24eed4f1f33a7ee3c2784",
		Success: true,
	}

	if diff := cmp.Diff(expectedResponse, parsedResponse); diff != "" {
		t.Error(diff)
	}
}

func TestHandleStatusWhenNotFound(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/index/0", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose
	require.Equal(t, http.StatusNotFound, resp.StatusCode)
}

func TestHandleStatusWhenRepoIdCannotBeParsed(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/index/FOO", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: t.TempDir(),
	}

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose
	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestHandleTruncate(t *testing.T) {
	req := httptest.NewRequest("POST", "/indexer/truncate", nil)
	w := httptest.NewRecorder()

	dir := t.TempDir()
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.createIndexDir()

	filesToDelete := []string{
		"700_v16.00000.zoekt",
		"700_v16.00000.zoekt.meta",
		"700_v16.00703.zoekt.3115102821.tmp",
		"707_v16.00000.zoekt",
		"707_v16.00000.zoekt.meta",
		"zoekt-builder-shard-log.tsv",
	}

	if err := createTestFiles(dir, filesToDelete); err != nil {
		require.NoError(t, err)
	}

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	existingFiles := filterExistingFiles(dir, filesToDelete)

	require.Empty(t, existingFiles)
}

func TestHandleTruncateWhenLocked(t *testing.T) {
	req := httptest.NewRequest("POST", "/indexer/truncate", nil)
	w := httptest.NewRecorder()

	dir := t.TempDir()
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}
	server.initMetrics()

	server.IndexingLock.TryLock(700)
	defer server.IndexingLock.Unlock(700)

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"700_v16.00001.zoekt",
		"7_v16.00000.zoekt",
		"7_v16.00001.zoekt",
		"7_v16.00001.zoekt.meta",
	}

	if err := createTestFiles(dir, filesToKeep); err != nil {
		require.NoError(t, err)
	}

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose

	require.Equal(t, http.StatusLocked, resp.StatusCode)

	existingFiles := filterExistingFiles(dir, filesToKeep)
	require.Equal(t, filesToKeep, existingFiles)
}

func TestHandleDebugLs(t *testing.T) {
	dir := t.TempDir()
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	// Create some test files
	testFiles := []string{
		"file1.txt",
		"file2.go",
		"subdir/file3.json",
	}

	for _, file := range testFiles {
		path := filepath.Join(dir, file)
		err := os.MkdirAll(filepath.Dir(path), 0o755)
		require.NoError(t, err)
		err = os.WriteFile(path, []byte("test content"), 0o644)
		require.NoError(t, err)
	}

	req := httptest.NewRequest("GET", "/indexer/debug/ls", nil)
	w := httptest.NewRecorder()

	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var response []debug_ls.FileInfo
	err := json.NewDecoder(resp.Body).Decode(&response)
	require.NoError(t, err)

	require.Len(t, response, len(testFiles))

	// Create a map of expected files for easier comparison
	expectedFiles := make(map[string]bool)
	for _, file := range testFiles {
		expectedFiles[filepath.Join(dir, file)] = true
	}

	// Verify each file in the response
	for _, file := range response {
		require.True(t, expectedFiles[file.Path], "Unexpected file: %s", file.Path)
		require.Positive(t, file.Size, "File size should be positive for %s", file.Path)
	}
}

func TestHandleDelete(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/indexer/index/7", nil)
	w := httptest.NewRecorder()

	dir := t.TempDir()
	server := defaultIndexServer()
	server.IndexBuilder = DefaultIndexBuilder{
		IndexDir: dir,
	}

	server.createIndexDir()
	tmpCleaner := file_cleaner.NewFileCleaner(server.IndexBuilder.GetIndexDir(), server.IndexingLock)
	tmpCleaner.Init()

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"707_v16.00000.zoekt",
		"707_v16.00000.zoekt.meta",
	}
	filesToDelete := []string{
		"7_v16.00000.zoekt",
		"7_v16.00001.zoekt",
		"7_v16.00001.zoekt.meta",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	if err := createTestFiles(dir, testFiles); err != nil {
		require.NoError(t, err)
	}

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	tmpCleaner.Clean(context.Background())

	existingFiles := filterExistingFiles(dir, testFiles)

	require.Equal(t, filesToKeep, existingFiles)
}

func TestHandleDeleteWhenIndexingIsLocked(t *testing.T) {
	dir := t.TempDir()
	server := defaultIndexServer()
	startChannel := make(chan interface{})
	doneChannel := make(chan interface{})
	server.IndexBuilder = indexBuilderMock{
		IndexDir:     dir,
		block:        true,
		doneChannel:  doneChannel,
		startChannel: startChannel,
	}
	server.initMetrics()

	router := server.Router()

	body := `{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`
	r1 := strings.NewReader(body)
	req1 := httptest.NewRequest("POST", "/indexer/index", r1)
	req2 := httptest.NewRequest("DELETE", "/indexer/index/7", nil)

	w1 := httptest.NewRecorder()
	w2 := httptest.NewRecorder()

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()

		router.ServeHTTP(w1, req1)
	}()
	go func() {
		defer wg.Done()

		<-startChannel
		router.ServeHTTP(w2, req2)
		close(doneChannel)
	}()

	wg.Wait()

	resp1 := w1.Result()
	defer resp1.Body.Close()
	resp2 := w2.Result() //nolint:bodyclose

	require.Equal(t, http.StatusOK, resp1.StatusCode)
	require.Equal(t, http.StatusLocked, resp2.StatusCode)
}

func TestHandleDeleteIndexIdCannotBeParsed(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/indexer/index/FOO", nil)
	w := httptest.NewRecorder()
	server := defaultIndexServer()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose

	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestHandleIndex(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: t.TempDir(),
	}
	server.createIndexDir()

	server.initMetrics()

	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleIndexWhenLocked(t *testing.T) {
	body := `{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`
	r1 := strings.NewReader(body)
	req1 := httptest.NewRequest("POST", "/indexer/index", r1)

	r2 := strings.NewReader(body)
	req2 := httptest.NewRequest("POST", "/indexer/index", r2)

	server := defaultIndexServer()
	startChannel := make(chan interface{})
	doneChannel := make(chan interface{})
	server.IndexBuilder = indexBuilderMock{
		IndexDir:     t.TempDir(),
		block:        true,
		doneChannel:  doneChannel,
		startChannel: startChannel,
	}
	server.initMetrics()

	router := server.Router()

	w1 := httptest.NewRecorder()
	w2 := httptest.NewRecorder()

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()

		router.ServeHTTP(w1, req1)
	}()
	go func() {
		defer wg.Done()

		<-startChannel
		router.ServeHTTP(w2, req2)
		close(doneChannel)
	}()

	wg.Wait()

	resp1 := w1.Result()
	defer resp1.Body.Close()
	resp2 := w2.Result() //nolint:bodyclose

	require.Equal(t, http.StatusOK, resp1.StatusCode)
	require.Equal(t, http.StatusLocked, resp2.StatusCode)
}

func TestHandleIndexWhenParseRequestErr(t *testing.T) {
	r := strings.NewReader(`{"nonExistingField": true}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: t.TempDir(),
	}
	server.createIndexDir()

	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose

	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestHandleIndexWhenIndexingErr(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir:           t.TempDir(),
		repositoryResponse: errors.New("error"),
	}
	server.createIndexDir()

	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result() //nolint:bodyclose

	require.Equal(t, http.StatusInternalServerError, resp.StatusCode)
}

func TestHandleIndexWhenTimeoutErr(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.IndexBuilder = DefaultIndexBuilder{
		IndexDir: t.TempDir(),
	}
	server.createIndexDir()

	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	type response struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}

	var parsedResp response
	err := json.NewDecoder(resp.Body).Decode(&parsedResp)

	require.NoError(t, err)
	require.Equal(t, http.StatusGatewayTimeout, resp.StatusCode)
	require.Equal(t, "context deadline exceeded", parsedResp.Error)
}

func TestSuccessfulSearch(t *testing.T) {
	// Create mock servers with different responses
	server1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(search.ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{
					{FileName: "file1.go", Score: 0.5},
				},
			},
		})
	}))
	defer server1.Close()

	server2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(search.ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{
					{FileName: "file2.go", Score: 0.8},
				},
			},
		})
	}))
	defer server2.Close()

	// Create a search request
	req := httptest.NewRequest("POST", "/indexer/proxy_search", strings.NewReader(`{
		"Q": "test",
		"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
		"RepoIds": [1, 2],
		"Timeout": "1s",
		"ForwardTo": [{"Endpoint": "`+server1.URL+`"}, {"Endpoint": "`+server2.URL+`"}]
	}`))
	req.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	defer resp.Body.Close()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var result search.SearchResult
	err := json.NewDecoder(resp.Body).Decode(&result)
	require.NoError(t, err)

	require.Len(t, result.Result.Files, 2)
	require.Equal(t, "file2.go", result.Result.Files[0].FileName)
	require.Equal(t, "file1.go", result.Result.Files[1].FileName)
	require.Empty(t, result.Failures)
}

func TestHandleSearchWithError(t *testing.T) {
	// Create a mock server that returns an error
	errorServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}))
	defer errorServer.Close()

	// Create a search request with the error-producing server
	req := httptest.NewRequest("POST", "/indexer/proxy_search", strings.NewReader(`{
		"Q": "test",
		"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
		"RepoIds": [1, 2],
		"Timeout": "1s",
		"ForwardTo": [{"Endpoint": "`+errorServer.URL+`"}]
	}`))
	req.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.initMetrics()
	router := server.Router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	type response struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}

	var parsedResp response
	err := json.NewDecoder(resp.Body).Decode(&parsedResp)

	require.NoError(t, err)
	require.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	require.Contains(t, parsedResp.Error, "all searches failed")
}
