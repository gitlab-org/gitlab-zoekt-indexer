package server

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/middleware_logger"
)

func (s *IndexServer) Router() *chi.Mux {
	r := chi.NewRouter()
	logger := middleware_logger.SetUpLogger()
	r.Use(middleware_logger.SlogMiddleware(logger))
	r.Use(middleware.Heartbeat(s.getPath("/health")))

	r.Get(s.getPath("/index/{id}"), s.handleStatus())
	r.Delete(s.getPath("/index/{id}"), s.handleDelete())
	r.Post(s.getPath("/index"), s.handleIndex())
	r.Post(s.getPath("/truncate"), s.handleTruncate())
	r.Get(s.getPath("/metrics"), s.handleMetrics())
	r.Get(s.getPath("/debug/ls"), s.handleDebugLs())
	r.Post(s.getPath("/proxy_search"), s.handleSearch())

	return r
}

func (s *IndexServer) getPath(path string) string {
	return s.PathPrefix + path
}
