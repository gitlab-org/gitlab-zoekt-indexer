package task_request_response_test

import (
	"context"
	"encoding/json"
	"net/http"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/search"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/server"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/task_request_response"
	"go.uber.org/goleak"
)

const (
	deleteRepoId  = 10
	deleteRepoId2 = 20
	indexRepoId   = 33
	indexRepoId2  = 40
	pathPrefix    = "/indexer"
)

func TestProcess(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(indexRepoId),
			},
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(indexRepoId2),
			},
			{
				"name": "delete",
				"payload": map[string]interface{}{
					"RepoId": deleteRepoId,
					"Callback": map[string]interface{}{
						"name": "delete",
						"payload": map[string]interface{}{
							"task_id": "100",
						},
					},
				},
			},
			{
				"name": "delete",
				"payload": map[string]interface{}{
					"RepoId": deleteRepoId2,
					"Callback": map[string]interface{}{
						"name": "delete",
						"payload": map[string]interface{}{
							"task_id": "100",
						},
					},
				},
			},
		},
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	indexReq := buildIndexRequest(buildIndexTaskPayload(indexRepoId))
	indexReq2 := buildIndexRequest(buildIndexTaskPayload(indexRepoId2))
	s.IndexingLock.TryLock(indexRepoId2)
	s.IndexingLock.TryLock(deleteRepoId2)
	mockIndexBuilderInstance.On("IndexRepository", context.Background(), indexReq, mock.AnythingOfType("callback.CallbackFunc")).Once()
	mockIndexBuilderInstance.On("DeleteRepository", deleteReq(deleteRepoId), mock.AnythingOfType("callback.CallbackFunc")).Once()
	mockIndexBuilderInstance.wg.Add(2)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	mockIndexBuilderInstance.wg.Wait()
	mockIndexBuilderInstance.AssertExpectations(t)
	mockIndexBuilderInstance.AssertNotCalled(t, "IndexRepository", context.Background(), indexReq2, mock.AnythingOfType("callback.CallbackFunc"))
	mockIndexBuilderInstance.AssertNotCalled(t, "DeleteRepository", deleteReq(deleteRepoId2), mock.AnythingOfType("callback.CallbackFunc"))
}

func TestProcessPullFrequencyPresentInBody(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(indexRepoId),
			},
		},
		"pull_frequency": "500ms",
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	indexReq := buildIndexRequest(buildIndexTaskPayload(indexRepoId))
	mockIndexBuilderInstance.On("IndexRepository", context.Background(), indexReq, mock.AnythingOfType("callback.CallbackFunc")).Once()
	mockIndexBuilderInstance.wg.Add(1)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, 500*time.Millisecond, processResult.Interval)
	mockIndexBuilderInstance.wg.Wait()
	mockIndexBuilderInstance.AssertExpectations(t)
}

func TestProcessPullFrequencyPresentInvalidBody(t *testing.T) {
	t.Parallel()

	bodyJson := `{"name": "foo" "status": bar}`

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	indexReq := buildIndexRequest(buildIndexTaskPayload(indexRepoId))
	s.IndexingLock.TryLock(indexRepoId)
	processResult := task_request_response.Process(context.Background(), []byte(bodyJson), s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	time.Sleep(1 * time.Second)
	mockIndexBuilderInstance.AssertNotCalled(t, "IndexRepository", context.Background(), indexReq, mock.AnythingOfType("callback.CallbackFunc"))
}

func TestProcessDeleteTask(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name": "delete",
				"payload": map[string]interface{}{
					"RepoId": deleteRepoId,
					"Callback": map[string]interface{}{
						"name": "delete",
						"payload": map[string]interface{}{
							"task_id": "100",
						},
					},
				},
			},
		},
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	mockIndexBuilderInstance.On("DeleteRepository", deleteReq(deleteRepoId), mock.AnythingOfType("callback.CallbackFunc")).Once()
	mockIndexBuilderInstance.wg.Add(1)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	mockIndexBuilderInstance.wg.Wait()
	mockIndexBuilderInstance.AssertExpectations(t)
}

func TestProcessDeleteTaskWhenLocked(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name": "delete",
				"payload": map[string]interface{}{
					"RepoId": deleteRepoId,
					"Callback": map[string]interface{}{
						"name": "delete",
						"payload": map[string]interface{}{
							"task_id": "100",
						},
					},
				},
			},
		},
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	s.IndexingLock.TryLock(deleteRepoId)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	time.Sleep(1 * time.Second)
	mockIndexBuilderInstance.AssertNotCalled(t, "DeleteRepository", deleteReq(deleteRepoId), mock.AnythingOfType("callback.CallbackFunc"))
}

func TestProcessIndexTask(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(indexRepoId),
			},
		},
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	indexReq := buildIndexRequest(buildIndexTaskPayload(indexRepoId))
	mockIndexBuilderInstance.On("IndexRepository", context.Background(), indexReq, mock.AnythingOfType("callback.CallbackFunc")).Once()
	mockIndexBuilderInstance.wg.Add(1)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	mockIndexBuilderInstance.wg.Wait()
	mockIndexBuilderInstance.AssertExpectations(t)
}

func TestProcessIndexTaskWhenLocked(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(indexRepoId),
			},
		},
	}
	bodyJson := bodyJson(body)

	mockIndexBuilderInstance := new(MockIndexBuilder)
	s := defaultIndexServer()
	s.IndexBuilder = mockIndexBuilderInstance
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	indexReq := buildIndexRequest(buildIndexTaskPayload(indexRepoId))
	s.IndexingLock.TryLock(indexRepoId)
	processResult := task_request_response.Process(context.Background(), bodyJson, s)
	require.Equal(t, time.Duration(0), processResult.Interval)
	time.Sleep(1 * time.Second)
	mockIndexBuilderInstance.AssertNotCalled(t, "IndexRepository", context.Background(), indexReq, mock.AnythingOfType("callback.CallbackFunc"))
}

func buildIndexTaskPayload(repoID uint32) map[string]any {
	result := make(map[string]any)
	result["GitalyConnectionInfo"] = map[string]interface{}{
		"Address": "address",
		"Token":   "token",
		"Storage": "repository_storage",
	}
	result["Callback"] = map[string]interface{}{
		"name": "index",
		"payload": map[string]interface{}{
			"project_id": repoID,
		},
	}
	result["RepoId"] = repoID
	result["FileSizeLimit"] = 5
	result["Timeout"] = "10s"
	return result
}

func buildIndexRequest(data map[string]any) server.IndexRequest {
	var indexReq server.IndexRequest
	dataJson, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	_ = json.Unmarshal(dataJson, &indexReq)
	return indexReq
}

func defaultIndexServer() *server.IndexServer {
	return &server.IndexServer{
		PathPrefix:   pathPrefix,
		IndexingLock: indexing_lock.NewIndexingLock(),
		Searcher: search.NewSearcher(&http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}),
		CallbackApi: callback.CallbackApi{
			Client: &http.Client{},
		},
	}
}

func bodyJson(body map[string]interface{}) []byte {
	out, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}
	return out
}

func deleteReq(repoID uint32) server.DeleteRequest {
	return server.DeleteRequest{
		RepoID: repoID,
		Callback: &callback.CallbackParams{
			Name: "delete",
			RailsPayload: map[string]interface{}{
				"task_id": "100",
			},
		},
	}
}

type MockIndexBuilder struct {
	mu sync.Mutex
	wg sync.WaitGroup
	mock.Mock
}

func (m *MockIndexBuilder) GetIndexDir() string {
	m.Called()
	return "index_dir"
}

func (m *MockIndexBuilder) IndexRepository(ctx context.Context, req server.IndexRequest, callbackFunc callback.CallbackFunc) error {
	defer m.wg.Done()
	m.mu.Lock()
	m.Called(ctx, req, callbackFunc)
	m.mu.Unlock()
	return nil
}

func (m *MockIndexBuilder) DeleteRepository(req server.DeleteRequest, callbackFunc callback.CallbackFunc, lock *indexing_lock.IndexingLock) error {
	defer m.wg.Done()
	m.mu.Lock()
	m.Called(req, callbackFunc)
	m.mu.Unlock()
	return nil
}

func TestMain(m *testing.M) {
	// Ignore glog goleaks since it is indirect
	goleak.VerifyTestMain(m, goleak.IgnoreTopFunction("github.com/golang/glog.(*fileSink).flushDaemon"))
}
