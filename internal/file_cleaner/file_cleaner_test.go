package file_cleaner

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"go.uber.org/goleak"
)

func TestCleanTmpFiles(t *testing.T) {
	dir := t.TempDir()

	tmpCleaner := FileCleaner{
		IndexDir:     dir,
		Ticker:       time.NewTicker(10 * time.Minute),
		IndexingLock: indexing_lock.NewIndexingLock(),
		removeFunc:   os.Remove,
	}

	if err := os.MkdirAll(dir, 0o755); err != nil {
		t.Fatal(err)
	}

	locked := tmpCleaner.IndexingLock.TryLock(7)
	require.True(t, locked)

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"707_v16.00000.zoekt",
		"7_v16.00702.zoekt.3269846088.tmp",
	}
	filesToDelete := []string{
		"700_v16.00703.zoekt.3115102821.tmp",
		"707_v16.00703.zoekt.3115102821.tmp",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := tmpCleaner.cleanTmpFiles(context.Background())

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func TestCleanTmpFilesWithAFailureContinues(t *testing.T) {
	fileToBeDeleted1 := "700_v16.00703.zoekt.3115102821.tmp"
	fileToBeDeletedWillFail := "700_v16.00704.zoekt.3115102821.tmp"
	fileToBeDeleted2 := "707_v16.00703.zoekt.3115102821.tmp"

	var deletedPaths []string
	var removeFunc = func(name string) error {
		name = path.Base(name)
		deletedPaths = append(deletedPaths, name)
		if name == fileToBeDeletedWillFail {
			return errors.New("Failed to delete a file")
		} else {
			return nil
		}
	}

	dir := t.TempDir()

	tmpCleaner := FileCleaner{
		IndexDir:     dir,
		Ticker:       time.NewTicker(10 * time.Minute),
		IndexingLock: indexing_lock.NewIndexingLock(),
		removeFunc:   removeFunc,
	}

	if err := os.MkdirAll(dir, 0o755); err != nil {
		t.Fatal(err)
	}

	testFiles := []string{fileToBeDeleted1, fileToBeDeletedWillFail, fileToBeDeleted2}

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := tmpCleaner.cleanTmpFiles(context.Background())

	require.Equal(t, []string{fileToBeDeleted1, fileToBeDeletedWillFail, fileToBeDeleted2}, deletedPaths)
	require.Error(t, err)

}

func TestMarkRepoForDeletion(t *testing.T) {
	dir := t.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)
	fileCleaner.MarkRepoForDeletion(7)
	fileCleaner.MarkRepoForDeletion(700)

	filesToCreate := []string{
		"repos_to_delete/7.delete",
		"repos_to_delete/700.delete",
	}

	var existingFiles []string

	for _, file := range filesToCreate {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToCreate, existingFiles)
}

func TestRemoveDeletedRepos(t *testing.T) {
	dir := t.TempDir()

	tmpCleaner := NewFileCleaner(dir, nil)
	tmpCleaner.Init()

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"707_v16.00000.zoekt",
		"707_v16.00000.zoekt.meta",
	}
	filesToDelete := []string{
		"7_v16.00703.zoekt",
		"7_v16.00703.zoekt",
		"7_v16.00000.zoekt.meta",
		"repos_to_delete/7.delete",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	require.NoError(t, tmpCleaner.removeDeletedRepos(context.Background()))

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func TestRemoveShardsFor(t *testing.T) {
	dir := t.TempDir()

	tmpCleaner := NewFileCleaner(dir, nil)

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"707_v16.00000.zoekt",
		"707_v16.00000.zoekt.meta",
	}
	filesToDelete := []string{
		"7_v16.00703.zoekt",
		"7_v16.00703.zoekt",
		"7_v16.00000.zoekt.meta",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := tmpCleaner.RemoveShardsFor(7)

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func TestRemoveShards(t *testing.T) {
	dir := t.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)
	fileCleaner.Init()

	filesToKeep := []string{
		"700_v16.00000.zoekt",
		"707_v16.00000.zoekt",
		"707_v16.00000.zoekt.meta",
		"repos_to_delete/7.delete",
	}
	filesToDelete := []string{
		"7_v16.00703.zoekt",
		"7_v16.00703.zoekt",
		"7_v16.00000.zoekt.meta",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := fileCleaner.removeShards(context.Background(), 7)

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func TestTruncateExceptUUID(t *testing.T) {
	dir := t.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)
	fileCleaner.Init()

	filesToKeep := []string{
		"node.uuid",
	}
	filesToDelete := []string{
		"7_v16.00703.zoekt",
		"7_v16.00703.zoekt",
		"7_v16.00000.zoekt.meta",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := fileCleaner.TruncateExceptUUID()

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func TestRemoveNodeUUID(t *testing.T) {
	dir := t.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)
	fileCleaner.Init()

	filesToKeep := []string{
		"7_v16.00703.zoekt",
		"7_v16.00703.zoekt",
		"7_v16.00000.zoekt.meta",
	}
	filesToDelete := []string{
		"node.uuid",
	}
	testFiles := append(filesToKeep, filesToDelete...)

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := fileCleaner.RemoveNodeUUID()

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, filesToKeep, existingFiles)
}

func setUpTestDir(b *testing.B, dir string, limit int) {
	for i := 0; i < limit; i++ {
		path := filepath.Join(dir, fmt.Sprintf("%d_v16.00000.zoekt", i))
		if err := os.WriteFile(path, []byte{}, 0644); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkMarkRepoForDeletion(b *testing.B) {
	dir := b.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)
	err := fileCleaner.Init()
	require.NoError(b, err)

	for i := 0; i < b.N; i++ {
		for repoID := 3000; repoID < 4096; repoID++ {
			fileCleaner.MarkRepoForDeletion(uint32(repoID))
		}
	}
}

func BenchmarkRemoveShardsFor(b *testing.B) {
	dir := b.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		setUpTestDir(b, dir, 4096)
		b.StartTimer()

		for repoID := 3000; repoID < 4096; repoID++ {
			fileCleaner.RemoveShardsFor(uint32(repoID))
		}
	}
}

func BenchmarkRemoveFiles(b *testing.B) {
	dir := b.TempDir()

	fileCleaner := NewFileCleaner(dir, nil)

	var repoIDs []uint32
	for repoID := 3000; repoID < 4096; repoID++ {
		repoIDs = append(repoIDs, uint32(repoID))
	}

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		setUpTestDir(b, dir, 4096)
		b.StartTimer()

		fileCleaner.removeShards(context.Background(), repoIDs...)
	}
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
