package gitaly

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
)

const existingSHA = "existing_sha"

type zeroSHAHashFinder struct{}
type zeroSHA256HashFinder struct{}
type emptySHAHashFinder struct{}
type existingSHAHashFinder struct{}
type emptySHAHashFinderWithError struct{}

func (hf *zeroSHAHashFinder) getFromHash() string                        { return ZeroSHA }
func (hf *zeroSHAHashFinder) getDefaultSHAFromHash() (string, error)     { return existingSHA, nil }
func (hf *zeroSHA256HashFinder) getFromHash() string                     { return ZeroSHA256 }
func (hf *zeroSHA256HashFinder) getDefaultSHAFromHash() (string, error)  { return existingSHA, nil }
func (hf *emptySHAHashFinder) getFromHash() string                       { return "" }
func (hf *emptySHAHashFinder) getDefaultSHAFromHash() (string, error)    { return existingSHA, nil }
func (hf *existingSHAHashFinder) getFromHash() string                    { return existingSHA }
func (hf *existingSHAHashFinder) getDefaultSHAFromHash() (string, error) { return existingSHA, nil }
func (hf *emptySHAHashFinderWithError) getFromHash() string              { return "" }
func (hf *emptySHAHashFinderWithError) getDefaultSHAFromHash() (string, error) {
	return "", errors.New("test error")
}

func TestDetermineFromHash(t *testing.T) {
	tests := []struct {
		desc        string
		finder      HashFinder
		expected    string
		expectedErr error
	}{
		{"zeroSHA provided", &zeroSHAHashFinder{}, NullTreeSHA, nil},
		{"zeroSHA256 provided", &zeroSHA256HashFinder{}, NullTreeSHA256, nil},
		{"emptySHA provided", &emptySHAHashFinder{}, existingSHA, nil},
		{"SHA provided", &existingSHAHashFinder{}, existingSHA, nil},
		{"emptySHA provided with error", &emptySHAHashFinderWithError{}, "", errors.New("test error")},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()

			result, err := determineFromHash(tc.finder)
			if tc.expectedErr != nil {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, tc.expected, result)
		})
	}
}

func TestMain(m *testing.M) {
	// Ignore glog goleaks since it is indirect
	goleak.VerifyTestMain(m, goleak.IgnoreTopFunction("github.com/golang/glog.(*fileSink).flushDaemon"))
}
