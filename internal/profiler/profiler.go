package profiler

import (
	"log/slog"
	"os"

	"cloud.google.com/go/profiler"
	"google.golang.org/api/option"
)

type Profiler struct {
	start  func(cfg profiler.Config, options ...option.ClientOption) error
	getenv func(string) string
}

func NewProfiler() Profiler {
	return Profiler{
		start:  profiler.Start,
		getenv: os.Getenv,
	}
}

func (p *Profiler) Init(serviceName, version string) {
	if p.getenv("GOOGLE_CLOUD_PROFILER_ENABLED") == "" {
		return
	}
	err := p.start(profiler.Config{
		Service:        serviceName,
		ServiceVersion: version,
		MutexProfiling: true,
	})
	if err != nil {
		slog.Error("could not initialize profiler", "service", serviceName, "version", version, "err", err)
	}
}
