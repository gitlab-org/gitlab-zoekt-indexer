package profiler

import (
	"errors"
	"testing"

	"cloud.google.com/go/profiler"
	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
	"google.golang.org/api/option"
)

const serviceName = "test-service"

var wasStarted = false

func stubbedProfiler(t *testing.T, enabled bool, err error) Profiler {
	getenv := func(key string) string {
		require.Equal(t, "GOOGLE_CLOUD_PROFILER_ENABLED", key)
		if enabled {
			return "true"
		} else {
			return ""
		}
	}

	wasStarted = false
	start := func(cfg profiler.Config, options ...option.ClientOption) error {
		wasStarted = true
		require.Equal(t, serviceName, cfg.Service)
		require.Equal(t, "version123", cfg.ServiceVersion)

		return err
	}

	return Profiler{
		start:  start,
		getenv: getenv,
	}
}

func TestInitCallsStart(t *testing.T) {
	p := stubbedProfiler(t, true, nil)

	p.Init(serviceName, "version123")

	require.True(t, wasStarted)
}

func TestInitNotEnabledDoesNotCallStart(t *testing.T) {
	p := stubbedProfiler(t, false, nil)

	p.Init(serviceName, "version123")

	require.False(t, wasStarted)
}

func TestInitCallsStartWithErrorDoesNotCrash(t *testing.T) {
	p := stubbedProfiler(t, true, errors.New("Something broke"))

	p.Init(serviceName, "version123")

	require.True(t, wasStarted)
}

func TestNewProfilerReturnsAProfiler(t *testing.T) {
	profiler := NewProfiler()

	require.NotNil(t, profiler.start)
	require.NotNil(t, profiler.getenv)
}

func TestMain(m *testing.M) {
	opts := []goleak.Option{
		goleak.IgnoreTopFunction("go.opencensus.io/stats/view.(*worker).start"),
	}
	goleak.VerifyTestMain(m, opts...)
}
