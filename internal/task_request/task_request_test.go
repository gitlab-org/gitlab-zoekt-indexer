package task_request_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/task_request"
	"go.uber.org/goleak"
)

const (
	nodeName  = "nodeName"
	nodeUUID  = "3869fe21-36d1"
	version   = "dev"
	searchURL = "http://search.url"
	selfURL   = "http://self.url"
)

func TestNewTaskRequestTimerWhenSecretDoNotExist(t *testing.T) {
	indexDir := t.TempDir()
	secretPath := filepath.Join(indexDir, "secret.txt")

	_, err := task_request.NewTaskRequestTimer(&task_request.NewTaskRequestTimerParams{
		IndexDir:       indexDir,
		NodeName:       nodeName,
		SelfURL:        selfURL,
		SearchURL:      searchURL,
		GitlabURL:      "http://gitlab.url",
		SecretFilePath: secretPath,
	})

	expectedError := fmt.Sprintf("open %s: no such file or directory", secretPath)
	require.EqualError(t, err, expectedError)
}

func TestSendRequest(t *testing.T) {
	indexDir := t.TempDir()
	secretPath := filepath.Join(indexDir, "secret.txt")

	data := []byte("secret")
	err := os.WriteFile(secretPath, data, 0644)
	require.NoError(t, err)

	var requestedURL *url.URL

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestedURL = r.URL
		fmt.Fprint(w, "empty response")
	}))
	defer svr.Close()

	indexingLock := indexing_lock.NewIndexingLock()
	locked := indexingLock.TryLock(55)
	require.True(t, locked)

	requestTimer, err := task_request.NewTaskRequestTimer(&task_request.NewTaskRequestTimerParams{
		IndexDir:       indexDir,
		NodeName:       nodeName,
		Version:        version,
		SelfURL:        selfURL,
		SearchURL:      searchURL,
		GitlabURL:      svr.URL,
		SecretFilePath: secretPath,
		NodeUUID:       nodeUUID,
		IndexingLock:   indexingLock,
	})

	require.NoError(t, err)

	_, requestErr := requestTimer.SendRequest()
	require.NoError(t, requestErr)

	values, err := url.ParseQuery(requestedURL.RawQuery)
	require.NoError(t, err)

	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/tasks", url.PathEscape(nodeUUID))
	expectedTaskCount := "1"
	expectedConcurrency := "0" // runtime.GOMAXPROCS(0) returns zero when executed from tests

	require.Equal(t, expectedPath, requestedURL.Path)
	require.Equal(t, selfURL, values.Get("node.url"))
	require.Equal(t, searchURL, values.Get("node.search_url"))
	require.Equal(t, nodeName, values.Get("node.name"))
	require.Equal(t, version, values.Get("node.version"))
	require.Equal(t, expectedConcurrency, values.Get("node.concurrency"))
	require.Equal(t, expectedTaskCount, values.Get("node.task_count"))
	require.NotZero(t, values.Get("disk.all"))
	require.NotZero(t, values.Get("disk.free"))
	require.NotZero(t, values.Get("disk.indexed"))
	require.NotZero(t, values.Get("disk.used"))
}

func TestMain(m *testing.M) {
	// Ignore glog goleaks since it is indirect
	goleak.VerifyTestMain(m, goleak.IgnoreTopFunction("github.com/golang/glog.(*fileSink).flushDaemon"))
}
