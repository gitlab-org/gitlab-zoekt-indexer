package search

import (
	"net/http"
	"time"

	"github.com/sourcegraph/zoekt"
)

type Searcher struct {
	Client *http.Client
}

type SearchRequest struct {
	ZoektSearchRequest
	TimeoutString string `json:"Timeout"`
	Timeout       time.Duration
	ForwardTo     []Conn `json:"ForwardTo"`
	Headers       map[string]string
}

type Conn struct {
	Endpoint string `json:"Endpoint"`
}

type SearchResult struct {
	Result   Result          `json:"Result"`
	Error    string          `json:"Error"`
	Failures []SearchFailure `json:"Failures"`
	TimedOut bool            `json:"TimedOut"`
}

type SearchFailure struct {
	Error    string `json:"Error"`
	Endpoint string `json:"Endpoint"`
}

type Result struct {
	Files        []zoekt.FileMatch `json:"Files"`
	FileCount    uint32            `json:"FileCount"`
	MatchCount   uint32            `json:"MatchCount"`
	NgramMatches uint32            `json:"NgramMatches"`
}

type ZoektSearchRequest struct {
	Query   string        `json:"Q"`
	Options SearchOptions `json:"Opts"`
	RepoIds []uint32      `json:"RepoIds"`
}

type SearchOptions struct {
	TotalMaxMatchCount uint32 `json:"TotalMaxMatchCount"`
	NumContextLines    uint32 `json:"NumContextLines"`
}

type ZoektResponse struct {
	Result   zoekt.SearchResult
	Error    error
	Endpoint string
}
