package search

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/sourcegraph/zoekt"
	"github.com/stretchr/testify/require"
)

func TestSearchResultSorting(t *testing.T) {
	// Create mock servers with different responses
	server1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{
					{FileName: "file1.go", Score: 0.5},
					{FileName: "file2.go", Score: 0.8},
				},
			},
		})
	}))
	defer server1.Close()

	server2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{
					{FileName: "file3.go", Score: 0.9},
					{FileName: "file4.go", Score: 0.3},
				},
			},
		})
	}))
	defer server2.Close()

	// Create a search request with multiple endpoints
	req := http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2, 3],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "` + server1.URL + `"}, {"Endpoint": "` + server2.URL + `"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(&req)

	// Check that the search didn't return an error
	require.NoError(t, err)

	// Check that the result is not nil
	require.NotNil(t, result)

	// Check that the files are sorted by score in descending order
	require.Len(t, result.Result.Files, 4)
	for i := 0; i < len(result.Result.Files)-1; i++ {
		require.GreaterOrEqual(t, result.Result.Files[i].Score, result.Result.Files[i+1].Score)
	}

	// Check that the files are in the correct order
	require.Equal(t, "file3.go", result.Result.Files[0].FileName)
	require.Equal(t, "file2.go", result.Result.Files[1].FileName)
	require.Equal(t, "file1.go", result.Result.Files[2].FileName)
	require.Equal(t, "file4.go", result.Result.Files[3].FileName)
}

func TestSearchWithPartialFailure(t *testing.T) {
	// Create mock servers: one successful, one failing
	successServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{
					{FileName: "success.go", Score: 1.0},
				},
			},
		})
	}))
	defer successServer.Close()

	failureServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}))
	defer failureServer.Close()

	// Create a search request with both servers
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "` + successServer.URL + `"}, {"Endpoint": "` + failureServer.URL + `"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search didn't return an error
	require.NoError(t, err)

	// Check that the result is not nil
	require.NotNil(t, result)

	// Check that we have one successful result and one failure
	require.Len(t, result.Result.Files, 1)
	require.Len(t, result.Failures, 1)
}

func TestSearchAllServersFail(t *testing.T) {
	// Create mock servers that all return errors
	server1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}))
	defer server1.Close()

	server2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
	}))
	defer server2.Close()

	// Create a search request with both failing servers
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "` + server1.URL + `"}, {"Endpoint": "` + server2.URL + `"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search returned an error
	require.Error(t, err)
	require.Nil(t, result)

	// Check that the error message indicates all searches failed
	require.Contains(t, err.Error(), "all searches failed")
	require.Contains(t, err.Error(), "Internal Server Error")
	require.Contains(t, err.Error(), "Bad Gateway")

}

func TestSearchTimeoutWithNoResults(t *testing.T) {
	// Create a mock server that simulates a slow response with no results
	slowServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(200 * time.Millisecond)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Files: []zoekt.FileMatch{},
			},
		})
	}))
	defer slowServer.Close()

	// Create a search request with a very short timeout
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2, 3],
			"Timeout": "100ms",
			"ForwardTo": [{"Endpoint": "` + slowServer.URL + `"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search returned an error
	require.Error(t, err)
	require.Nil(t, result)

	// Check that the error message indicates a timeout
	require.EqualError(t, err, "multi node search failed: search timed out")
}

func TestNewSearchRequestDefaultTimeout(t *testing.T) {
	// Create a request with no timeout specified
	reqBody := `{
		"Q": "test query",
		"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
		"RepoIds": [1, 2, 3],
		"ForwardTo": [{"Endpoint": "http://example.com"}]
	}`
	req := httptest.NewRequest("POST", "/search", strings.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	searchReq, err := NewSearchRequest(req)
	require.NoError(t, err)
	require.Equal(t, defaultSearchTimeout, searchReq.TimeoutString)
}

func TestNewSearchRequestDefaultMaxResults(t *testing.T) {
	// Create a request with no TotalMaxMatchCount specified
	reqBody := `{
		"Q": "test query",
		"Opts": {"NumContextLines": 2
		},
		"RepoIds": [1, 2, 3],
		"ForwardTo": [{"Endpoint": "http://example.com"}]
	}`
	req := httptest.NewRequest("POST", "/search", strings.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	searchReq, err := NewSearchRequest(req)
	require.NoError(t, err)
	require.Equal(t, defaultMaxResults, searchReq.Options.TotalMaxMatchCount)
}

func TestNewSearchRequestCopiesBasicAuthHeaders(t *testing.T) {
	// Create a request with basic auth headers
	reqBody := `{
		"Q": "test query",
		"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
		"RepoIds": [1, 2, 3],
		"ForwardTo": [{"Endpoint": "http://example.com"}]
	}`
	req := httptest.NewRequest("POST", "/search", strings.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth("username", "password")

	searchReq, err := NewSearchRequest(req)
	require.NoError(t, err)

	// Check if the basic auth header was copied
	authHeader, exists := searchReq.Headers["Authorization"]
	require.True(t, exists)
	require.Equal(t, req.Header.Get("Authorization"), authHeader)
}

func TestSearchWithEmptyQuery(t *testing.T) {
	// Create a search request with an empty query
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2, 3],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "http://example.com"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search returned an error
	require.Error(t, err)
	require.Nil(t, result)

	// Check that the error message indicates an empty query
	require.EqualError(t, err, "error building search request: search query is empty")
}

func TestSearchWithNoForwardToConnections(t *testing.T) {
	// Create a search request with no forward to connections
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
				"Q": "",
				"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
				"RepoIds": [1, 2, 3],
				"Timeout": "1s",
				"ForwardTo": []
			}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search returned an error
	require.Error(t, err)
	require.Nil(t, result)

	// Check that the error message indicates an empty query
	require.EqualError(t, err, "error building search request: no forward-to connections specified")
}

func TestSearchCannotParseRequestBody(t *testing.T) {
	// Create a search request with a field that can't be marshaled
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 10, "NumContextLines": 2},
			"RepoIds": [1, 2, 3],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "http://example.com"}],
			"UnmarshalableField": func() {}
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)

	// Check that the search returned an error
	require.Error(t, err)
	require.Nil(t, result)

	// Check that the error message indicates a parsing problem
	require.Contains(t, err.Error(), "failed to parse request body")
}

func TestSearchMaxResults(t *testing.T) {
	// Create mock servers with different responses
	server1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Stats: zoekt.Stats{
					FileCount: 3,
				},
				Files: []zoekt.FileMatch{
					{FileName: "file1.go", Score: 0.6},
					{FileName: "file2.go", Score: 0.7},
					{FileName: "file3.go", Score: 0.85},
				},
			},
		})
	}))
	defer server1.Close()

	server2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(200 * time.Millisecond)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Stats: zoekt.Stats{
					FileCount: 2,
				},
				Files: []zoekt.FileMatch{
					{FileName: "file4.go", Score: 0.89},
					{FileName: "file5.go", Score: 0.9},
				},
			},
		})
	}))

	defer server2.Close()

	server3 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(500 * time.Millisecond)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(ZoektResponse{
			Result: zoekt.SearchResult{
				Stats: zoekt.Stats{
					FileCount: 2,
				},
				Files: []zoekt.FileMatch{
					{FileName: "file6.go", Score: 0.95},
					{FileName: "file7.go", Score: 0.99},
				},
			},
		})
	}))
	defer server3.Close()

	// Create a search request with a max match count of 4
	req := &http.Request{
		Body: io.NopCloser(strings.NewReader(`{
			"Q": "test",
			"Opts": {"TotalMaxMatchCount": 4, "NumContextLines": 2},
			"RepoIds": [1, 2, 3],
			"Timeout": "1s",
			"ForwardTo": [{"Endpoint": "` + server1.URL + `"}, {"Endpoint": "` + server2.URL + `"}]
		}`)),
		Header: make(http.Header),
	}
	req.Header.Set("Content-Type", "application/json")

	result, err := testSearcher().Search(req)
	require.NoError(t, err)
	require.NotNil(t, result)
	require.Equal(t, uint32(4), result.Result.FileCount)

	// Check that we stop processing after we have enough results and we
	// still sort the results by score in descending order
	require.Equal(t, "file5.go", result.Result.Files[0].FileName)
	require.Equal(t, "file4.go", result.Result.Files[1].FileName)
	require.Equal(t, "file3.go", result.Result.Files[2].FileName)
	require.Equal(t, "file2.go", result.Result.Files[3].FileName)
}

func testSearcher() *Searcher {
	return NewSearcher(&http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	})
}
