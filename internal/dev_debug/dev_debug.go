package dev_debug

import "os"

const debugEnv = "ZOEKT_ENABLE_DEBUG_LOGGING"

var trueValues = []string{"1", "true"}

func IsEnabled() bool {
	val, ok := os.LookupEnv(debugEnv)

	if !ok {
		return false
	}

	for _, v := range trueValues {
		if v == val {
			return true
		}
	}

	return false
}
