package dev_debug_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/dev_debug"
	"go.uber.org/goleak"
)

func TestIsEnabled(t *testing.T) {
	testCases := []struct {
		isSet    bool
		value    string
		expected bool
	}{
		{isSet: false, value: "", expected: false},
		{isSet: true, value: "", expected: false},
		{isSet: true, value: "true", expected: true},
		{isSet: true, value: "false", expected: false},
	}

	for _, tc := range testCases {
		if tc.isSet {
			t.Setenv("ZOEKT_ENABLE_DEBUG_LOGGING", tc.value)
		}

		require.Equal(t, tc.expected, dev_debug.IsEnabled())
	}
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
