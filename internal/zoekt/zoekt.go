package zoekt

import (
	"fmt"

	"github.com/sourcegraph/zoekt"
	"github.com/sourcegraph/zoekt/build"
)

type RepositoryBranch struct {
	Name    string
	Version string
}

type Options struct {
	IndexDir   string
	ID         uint32
	IsDelta    bool
	RepoSource string
	SizeMax    int
	Branches   []RepositoryBranch
}

type Client struct {
	opts           *Options
	builderOptions *build.Options
}

func (rb *RepositoryBranch) castToZoekt() zoekt.RepositoryBranch {
	return zoekt.RepositoryBranch{
		Name:    rb.Name,
		Version: rb.Version,
	}
}

func NewZoektClient(opts *Options) *Client {
	return &Client{
		opts:           opts,
		builderOptions: defaultBuilderOptions(opts),
	}
}

func defaultBuilderOptions(opts *Options) *build.Options {
	branches := make([]zoekt.RepositoryBranch, 0, len(opts.Branches))

	for _, b := range opts.Branches {
		branches = append(branches, b.castToZoekt())
	}

	buildOpts := build.Options{
		IndexDir: opts.IndexDir,
		SizeMax:  opts.SizeMax,
		IsDelta:  opts.IsDelta,
		RepositoryDescription: zoekt.Repository{
			ID:       opts.ID,
			Name:     fmt.Sprint(opts.ID),
			Source:   opts.RepoSource,
			Branches: branches,
		}}

	buildOpts.SetDefaults()
	return &buildOpts
}

func (c *Client) AddFile(builder *build.Builder, path string, content []byte, size int64, tooLarge bool, branches []string) error {
	if tooLarge && !c.builderOptions.IgnoreSizeMax(path) {
		if err := builder.Add(zoekt.Document{
			SkipReason: fmt.Sprintf("file size %d exceeds maximum size %d", size, c.builderOptions.SizeMax),
			Name:       path,
			Branches:   branches,
		}); err != nil {
			return err
		}

		return nil
	}

	if err := builder.Add(zoekt.Document{
		Name:     path,
		Content:  content,
		Branches: branches,
	}); err != nil {
		return fmt.Errorf("error adding document with name %s: %w", path, err)
	}

	return nil
}

func (c *Client) IncrementalSkipIndexing() bool {
	opts := c.builderOptions

	return opts.IncrementalSkipIndexing()
}

func (c *Client) NewBuilder() (*build.Builder, error) {
	builder, err := build.NewBuilder(*c.builderOptions)
	if err != nil {
		return nil, fmt.Errorf("build.NewBuilder: %w", err)
	}

	return builder, nil
}

func (c *Client) GetCurrentSHA() (string, bool, error) {
	existingRepository, ok, err := c.findRepositoryMetadata()

	if err != nil {
		return "", ok, err
	}

	if !ok {
		return "", ok, nil
	}

	for _, branch := range existingRepository.Branches {
		if branch.Name == "HEAD" {
			return branch.Version, true, nil
		}
	}

	return "", true, nil
}

func (c *Client) findRepositoryMetadata() (*zoekt.Repository, bool, error) {
	opts := c.builderOptions
	existingRepository, _, ok, err := opts.FindRepositoryMetadata()

	if err != nil {
		return nil, ok, fmt.Errorf("failed to get repository metadata: %w", err)
	}

	// todo - is this explicit check needed or can the repository be returned
	if !ok {
		return nil, ok, nil
	}

	return existingRepository, ok, nil
}
