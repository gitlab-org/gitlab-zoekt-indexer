package internal_api_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	internal_api "gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/api"
	"go.uber.org/goleak"
)

const (
	Path      = "dummy"
	GitlabURL = "http://example.com"
	NodeUUID  = "3869fe21-36d1"
)

func TestNewRequest(t *testing.T) {
	params := internal_api.InternalApiRequestParams{
		Path:       Path,
		GitlabURL:  GitlabURL,
		BodyParams: `{"key": "value"}`,
		NodeUUID:   NodeUUID,
		Secret:     []byte("secret"),
	}
	req, err := internal_api.NewRequest(context.Background(), params)

	require.NoError(t, err)
	url := req.URL
	expectedUrl := fmt.Sprintf("%s/api/v4/internal/search/zoekt/%s/%s", GitlabURL, NodeUUID, Path)
	require.Equal(t, expectedUrl, url.String())
	require.NotNil(t, req.Body)
}

func TestNewRequestWithoutBodyParams(t *testing.T) {
	params := internal_api.InternalApiRequestParams{
		Path:      Path,
		GitlabURL: GitlabURL,
		NodeUUID:  NodeUUID,
		Secret:    []byte("secret"),
	}
	req, err := internal_api.NewRequest(context.Background(), params)

	require.NoError(t, err)
	url := req.URL
	expectedUrl := fmt.Sprintf("%s/api/v4/internal/search/zoekt/%s/%s", GitlabURL, NodeUUID, Path)
	require.Equal(t, expectedUrl, url.String())
	require.Nil(t, req.Body)
}

func TestNewRequestWhenParamsDoesNotHaveSecret(t *testing.T) {
	params := internal_api.InternalApiRequestParams{
		Path:       Path,
		BodyParams: `{"key": "value"}`,
		GitlabURL:  GitlabURL,
		NodeUUID:   NodeUUID,
		Secret:     []byte(""),
	}
	_, err := internal_api.NewRequest(context.Background(), params)

	require.EqualError(t, err, "secret is empty")
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
