package callback_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"go.uber.org/goleak"
)

const (
	nodeUUID = "3869fe21-36d1"
)

func TestSuccessCallbackRequestForIndex(t *testing.T) {
	indexDir := t.TempDir()
	data := []byte("secret")
	err := os.WriteFile(filepath.Join(indexDir, "secret.txt"), data, 0644)
	require.NoError(t, err)
	var requestedURL url.URL
	var requestedBody interface{}
	svr := stubServer(&requestedURL, &requestedBody)
	defer svr.Close()
	callbackApiInstance, err := callbackApiInstance(svr.URL, indexDir)
	require.NoError(t, err)
	callbackApiInstance.SendSuccess(context.Background(), defaultCallbackParams(), indexDir, 1)
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))
	require.Equal(t, expectedPath, requestedURL.Path)
	requestedBodyMap := requestedBody.(map[string]interface{})
	require.Equal(t, requestedBodyMap["name"], defaultCallbackParams().Name)
	require.True(t, requestedBodyMap["success"].(bool))
	assert.Contains(t, requestedBodyMap, "additional_payload")
	assert.Contains(t, requestedBodyMap["additional_payload"], "repo_stats")
	repoStatsMap := requestedBodyMap["additional_payload"].(map[string]interface{})["repo_stats"].(map[string]interface{})
	assert.Contains(t, repoStatsMap, "size_in_bytes")
	assert.Contains(t, repoStatsMap, "index_file_count")
	assert.Contains(t, requestedBodyMap["payload"], "key")
}

func TestFailureCallbackRequestForIndex(t *testing.T) {
	indexDir := t.TempDir()
	data := []byte("secret")
	err := os.WriteFile(filepath.Join(indexDir, "secret.txt"), data, 0644)
	require.NoError(t, err)

	var requestedURL url.URL
	var requestedBody interface{}
	svr := stubServer(&requestedURL, &requestedBody)
	defer svr.Close()
	callbackApiInstance, err := callbackApiInstance(svr.URL, indexDir)
	require.NoError(t, err)
	failureReason := errors.New("Sample Error")
	callbackApiInstance.SendFailure(context.Background(), defaultCallbackParams(), indexDir, 1, failureReason)
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))
	require.Equal(t, expectedPath, requestedURL.Path)
	requestedBodyMap := requestedBody.(map[string]interface{})
	require.Equal(t, requestedBodyMap["name"], defaultCallbackParams().Name)
	require.False(t, requestedBodyMap["success"].(bool))
	require.Equal(t, requestedBodyMap["error"], failureReason.Error())
	assert.Contains(t, requestedBodyMap, "additional_payload")
	assert.Contains(t, requestedBodyMap["additional_payload"], "repo_stats")
	repoStatsMap := requestedBodyMap["additional_payload"].(map[string]interface{})["repo_stats"].(map[string]interface{})
	assert.Contains(t, repoStatsMap, "size_in_bytes")
	assert.Contains(t, repoStatsMap, "index_file_count")
	assert.Contains(t, requestedBodyMap["payload"], "key")
}

func TestNewCallbackApiWhenSecretDoNotExist(t *testing.T) {
	indexDir := t.TempDir()
	var requestedURL url.URL
	var requestedBody interface{}
	svr := stubServer(&requestedURL, &requestedBody)
	defer svr.Close()
	_, err := callbackApiInstance(svr.URL, indexDir)
	require.Error(t, err)
	require.Contains(t, err.Error(), "could not read secret from the file_path")
}

func TestNewCallbackApiWithTrailingNewline(t *testing.T) {
	// Temporary directory for test files
	indexDir := t.TempDir()
	secretPath := filepath.Join(indexDir, "secret.txt")

	// Input and expected secret
	secret := "test-secret"
	secretContent := []byte("\n\t " + secret + " \t\n")

	// Write the secret with extra whitespace to the file
	err := os.WriteFile(secretPath, secretContent, 0600)
	require.NoError(t, err, "Failed to write secret content to file")

	// Setup a stub server
	var requestedURL url.URL
	var requestedBody interface{}
	svr := stubServer(&requestedURL, &requestedBody)
	defer svr.Close()

	// Call the function under test
	api, err := callbackApiInstance(svr.URL, indexDir)
	require.NoError(t, err, "callbackApiInstance returned an error")
	require.NotNil(t, api, "callbackApiInstance returned nil")

	// Convert the byte slice to string for comparison
	actualSecret := string(api.Secret)
	require.Equal(t, secret, actualSecret, "The secret was not trimmed correctly")
}

func callbackApiInstance(internalUrl string, indexDir string) (callback.CallbackApi, error) {
	return callback.NewCallbackApi(internalUrl, nodeUUID, filepath.Join(indexDir, "secret.txt"), &http.Client{})
}

func defaultCallbackParams() callback.CallbackParams {
	return callback.CallbackParams{
		Name: "index",
		RailsPayload: map[string]interface{}{
			"key": "value",
		},
	}
}

func stubServer(svrRequestedPath *url.URL, requestedBody interface{}) *httptest.Server {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		*svrRequestedPath = *r.URL
		buf, _ := io.ReadAll(r.Body)
		err := json.Unmarshal(buf, &requestedBody)
		if err != nil {
			panic(err)
		}

	}))
	return svr
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
