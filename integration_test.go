package main_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
	gitalyauth "gitlab.com/gitlab-org/gitaly/v16/auth"
	gitalyClient "gitlab.com/gitlab-org/gitaly/v16/client"
	pb "gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/disk_stats"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexer"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/search"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/server"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/task_request_response"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/webserver_test_helper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	gitalyConnectionAddress = "tcp://localhost:8075"
	gitalyConnectionStorage = "default"
	nodeUUID                = "3869fe21-36d1"
	pathPrefix              = "/indexer"
	webserverPort1          = iota + 6091
	webserverPort2
	webserverPort3
	webserverPort4
)

var (
	gitalyConnInfo *gitalyConnectionInfo
)

type gitalyConnectionInfo struct {
	Address string `json:"address"`
	Storage string `json:"storage"`
	Token   string `json:"token"`
}

type testRepository struct {
	HeadSHA       string
	ProjectID     uint32
	Repo          string
	ProjectPath   string
	RepoPath      string
	RepoNamespace string
}

var (
	RepoA = testRepository{
		HeadSHA:       "b83d6e391c22777fca1ed3012fce84f633d7fed0",
		ProjectID:     72724,
		Repo:          "gitlab-org/gitlab-test.git",
		ProjectPath:   "gitlab-org/gitlab-test",
		RepoPath:      "https://gitlab.com/gitlab-org/gitlab-test.git",
		RepoNamespace: "gitlab-org",
	}
	RepoB = testRepository{
		HeadSHA:       "7c0330d9cc65704555672704a31ee7fafde12534",
		ProjectID:     47438270,
		Repo:          "gitlab-org/search-team/test-public-project.git",
		ProjectPath:   "search-team/test-public-project",
		RepoPath:      "https://gitlab.com/gitlab-org/search-team/test-public-project.git",
		RepoNamespace: "search-team",
	}
	EmptyRepo = testRepository{
		ProjectID:     60372649,
		Repo:          "gitlab-org/search-team/test-empty-project.git",
		ProjectPath:   "search-team/test-empty-project",
		RepoPath:      "https://gitlab.com/gitlab-org/search-team/test-empty-project.git",
		RepoNamespace: "search-team",
	}
	NonExistingRepo = testRepository{
		ProjectID:     0,
		Repo:          "nonexisting",
		ProjectPath:   "nonexisting",
		RepoPath:      "nonexisting",
		RepoNamespace: "nonexisting",
	}
	RepoSHA256 = testRepository{
		HeadSHA:       "38046b7d323a0efd18477213cfe5bf4a48280d2b0ce39d54a0b0288fcc249aa8",
		ProjectID:     56792528,
		Repo:          "gitlab-org/search-team/test-public-project-sha256.git",
		ProjectPath:   "search-team/test-public-project-sha256",
		RepoPath:      "https://gitlab.com/gitlab-org/search-team/test-public-project-sha256.git",
		RepoNamespace: "search-team",
	}
)

func TestMain(m *testing.M) {
	integration := os.Getenv("INTEGRATION")
	if integration != "" && integration != "false" {
		if err := ensureGitalyRepositories(EmptyRepo, RepoA, RepoB, RepoSHA256); err != nil {
			panic(err)
		}

		m.Run()
	}
}

func init() {
	gci, exists := os.LookupEnv("GITALY_CONNECTION_INFO")
	if exists {
		err := json.Unmarshal([]byte(gci), &gitalyConnInfo)
		if err != nil {
			panic(err)
		}
	} else {
		gitalyConnInfo = &gitalyConnectionInfo{
			Address: gitalyConnectionAddress,
			Storage: gitalyConnectionStorage,
		}
	}
}

func webserverSearchOnce(indexDir string, port int, repoID uint32, term string) (webserver_test_helper.SearchResponse, error) {
	webserver := webserver_test_helper.NewWebserverHelper(indexDir, port)

	cancel, err := webserver.Start()
	defer cancel()
	if err != nil {
		return webserver_test_helper.SearchResponse{}, err
	}

	searchResult, err := webserver.Search(repoID, term)
	if err != nil {
		return webserver_test_helper.SearchResponse{}, err
	}

	return searchResult, nil
}

func ensureGitalyRepositories(repos ...testRepository) error {
	var wg sync.WaitGroup
	wg.Add(len(repos))

	errorChan := make(chan error, len(repos))

	for _, repo := range repos {
		go func(r testRepository) {
			defer wg.Done()

			errorChan <- ensureGitalyRepository(r)
		}(repo)
	}

	wg.Wait()
	close(errorChan)

	errs := make([]error, 0, len(errorChan))
	for e := range errorChan {
		errs = append(errs, e)
	}

	return errors.Join(errs...)
}

func ensureGitalyRepository(repo testRepository) error {
	RPCCred := gitalyauth.RPCCredentialsV2(gitalyConnInfo.Token)

	connOpts := append(
		gitalyClient.DefaultDialOpts,
		grpc.WithPerRPCCredentials(RPCCred),
	)

	conn, err := gitalyClient.Dial(gitalyConnInfo.Address, connOpts)

	if err != nil {
		return fmt.Errorf("did not connect: %w", err)
	}

	repositoryClient := pb.NewRepositoryServiceClient(conn)
	repository := &pb.Repository{
		StorageName:  gitalyConnInfo.Storage,
		RelativePath: repo.Repo,
	}

	// Remove the repository if it already exists, for consistency
	if repositoryExistsResponse, repositoryExistsErr := repositoryClient.RepositoryExists(context.Background(), &pb.RepositoryExistsRequest{
		Repository: repository,
	}); repositoryExistsErr != nil {
		return repositoryExistsErr
	} else if repositoryExistsResponse.Exists {
		if _, err = repositoryClient.RemoveRepository(context.Background(), &pb.RemoveRepositoryRequest{
			Repository: repository,
		}); err != nil {
			removeRepositoryStatus, ok := status.FromError(err)
			if !ok || !(removeRepositoryStatus.Code() == codes.NotFound && removeRepositoryStatus.Message() == "repository does not exist") {
				return fmt.Errorf("remove repository: %w", err)
			}
		}
	}

	glRepository := &pb.Repository{
		StorageName:   gitalyConnInfo.Storage,
		RelativePath:  repo.Repo,
		GlProjectPath: repo.ProjectPath,
	}

	createReq := &pb.CreateRepositoryFromURLRequest{
		Repository: glRepository,
		Url:        repo.RepoPath,
	}

	_, err = repositoryClient.CreateRepositoryFromURL(context.Background(), createReq)
	if err != nil {
		return err
	}

	if repo.ProjectID == EmptyRepo.ProjectID {
		return nil
	}

	writeHeadReq := &pb.WriteRefRequest{
		Repository: glRepository,
		Ref:        []byte("refs/heads/master"),
		Revision:   []byte(repo.HeadSHA),
	}

	_, err = repositoryClient.WriteRef(context.Background(), writeHeadReq)

	return err
}

func getIndexFiles(indexDir string, repoID uint32) []string {
	files, err := disk_stats.GetIndexFiles(indexDir, fmt.Sprintf("%d_*", repoID))

	if err != nil {
		panic(err)
	}

	modifiedFiles := make([]string, 0, len(files))
	for _, path := range files {
		modifiedFiles = append(modifiedFiles, filepath.Base(path))
	}

	return modifiedFiles
}

func TestIndexRepositoryWhenGitalyConnectionInfoIsNil(t *testing.T) {
	t.Parallel()

	dir := t.TempDir()
	s := defaultIndexServer()

	indexBuilder := server.DefaultIndexBuilder{
		IndexDir: dir,
	}
	s.IndexBuilder = &indexBuilder
	indexRequest := server.IndexRequest{
		RepoID: RepoA.ProjectID,
		Callback: &callback.CallbackParams{
			Name: "index",
		},
		Timeout:       "30s",
		FileSizeLimit: 10000000,
		Force:         false,
	}

	indexError := indexBuilder.IndexRepository(context.Background(),
		indexRequest,
		callback.CallbackFunc{
			OnSuccess: func(params callback.CallbackParams) {
				s.CallbackApi.SendSuccess(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID)
			},
			OnFailure: func(params callback.CallbackParams, errorReason error) {
				s.CallbackApi.SendFailure(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID, errorReason)
			},
		},
	)
	require.EqualError(t, indexError, "gitalyConnectionInfo is not set")
}

func TestIndexRepositoryWhenUsingSHA256(t *testing.T) {
	t.Parallel()

	idxr := indexer.Indexer{
		IndexDir:           t.TempDir(),
		ProjectID:          RepoSHA256.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoSHA256.Repo,
		LimitFileSize:      10000,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := idxr.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 1,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedResult, result)

	sha, _, err := idxr.CurrentSHA()

	require.NoError(t, err)
	require.Equal(t, RepoSHA256.HeadSHA, sha)

	searchResult, err := webserverSearchOnce(idxr.IndexDir, webserverPort1, RepoSHA256.ProjectID, "SHA256")
	if err != nil {
		require.NoError(t, err)
	}

	require.Equal(t, 1, searchResult.Result.FileCount)
	expectedFiles := []string{"README.md"}
	require.Equal(t, expectedFiles, searchResult.FileNames())
}

func TestIndexRepositoryWhenFileSizeLimitIsNil(t *testing.T) {
	t.Parallel()

	dir := t.TempDir()
	s := defaultIndexServer()

	indexBuilder := server.DefaultIndexBuilder{
		IndexDir: dir,
	}
	s.IndexBuilder = &indexBuilder
	indexRequest := server.IndexRequest{
		RepoID: RepoA.ProjectID,
		Callback: &callback.CallbackParams{
			Name: "index",
		},
		GitalyConnectionInfo: new(server.GitalyConnectionInfo),
		Timeout:              "1s",
		Force:                false,
	}

	indexError := indexBuilder.IndexRepository(context.Background(),
		indexRequest,
		callback.CallbackFunc{
			OnSuccess: func(params callback.CallbackParams) {
				s.CallbackApi.SendSuccess(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID)
			},
			OnFailure: func(params callback.CallbackParams, errorReason error) {
				s.CallbackApi.SendFailure(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID, errorReason)
			},
		},
	)
	require.EqualError(t, indexError, "fileSizeLimit is not set")
}

func TestIndexRepositoryWhenTimeoutIsNotParsable(t *testing.T) {
	t.Parallel()

	dir := t.TempDir()
	s := defaultIndexServer()

	indexBuilder := server.DefaultIndexBuilder{
		IndexDir: dir,
	}
	s.IndexBuilder = &indexBuilder
	indexRequest := server.IndexRequest{
		RepoID: RepoA.ProjectID,
		Callback: &callback.CallbackParams{
			Name: "index",
		},
		GitalyConnectionInfo: new(server.GitalyConnectionInfo),
		Timeout:              "dummy",
		FileSizeLimit:        5000,
		Force:                false,
	}

	indexError := indexBuilder.IndexRepository(context.Background(),
		indexRequest,
		callback.CallbackFunc{
			OnSuccess: func(params callback.CallbackParams) {
				s.CallbackApi.SendSuccess(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID)
			},
			OnFailure: func(params callback.CallbackParams, errorReason error) {
				s.CallbackApi.SendFailure(context.Background(), params, s.IndexBuilder.GetIndexDir(), RepoA.ProjectID, errorReason)
			},
		},
	)
	require.EqualError(t, indexError, "failed to parse Timeout: dummy with error time: invalid duration \"dummy\"")
}

func TestIndexRepository(t *testing.T) {
	t.Parallel()

	idxr := indexer.Indexer{
		IndexDir:           t.TempDir(),
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := idxr.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 38,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedResult, result)

	sha, _, err := idxr.CurrentSHA()

	require.NoError(t, err)
	require.Equal(t, RepoA.HeadSHA, sha)

	searchResult, err := webserverSearchOnce(idxr.IndexDir, webserverPort2, RepoA.ProjectID, "test")
	if err != nil {
		require.NoError(t, err)
	}

	require.Equal(t, 3, searchResult.Result.FileCount)
	expectedFiles := []string{"encoding/test.txt", "custom-highlighting/test.gitlab-custom", "bar/branch-test.txt"}
	require.Equal(t, expectedFiles, searchResult.FileNames())
}

func TestIndexEmptyRepository(t *testing.T) {
	t.Parallel()

	idxr := indexer.Indexer{
		IndexDir:           t.TempDir(),
		ProjectID:          EmptyRepo.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: EmptyRepo.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := idxr.IndexRepository(context.Background())
	require.NoError(t, err)
	var expectedResult *indexer.IndexingResult // Empty pointer
	require.Equal(t, expectedResult, result)
}

func TestIncrementalIndexRepository(t *testing.T) {
	t.Parallel()

	indexDir := t.TempDir()
	incrementalIndexer := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		TargetSHA:          "e56497bb5f03a90a51293fc6d516788730953899",
		Writer:             &indexer.DefaultIndexWriter{},
	}

	incrementalResult, err := incrementalIndexer.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedIncrementalResult := &indexer.IndexingResult{
		ModifiedFilesCount: 29,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedIncrementalResult, incrementalResult)

	idxr := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := idxr.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 11,
		DeletedFilesCount:  1,
	}
	require.Equal(t, expectedResult, result)

	sha, _, err := idxr.CurrentSHA()

	require.NoError(t, err)
	require.Equal(t, RepoA.HeadSHA, sha)
}

func TestForceReindexing(t *testing.T) {
	t.Parallel()

	indexDir := t.TempDir()
	idxr := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	incrementalResult, err := idxr.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedIncrementalResult := &indexer.IndexingResult{
		ModifiedFilesCount: 38,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedIncrementalResult, incrementalResult)

	// Indexing a different project using the same projectID to simulate force push with git GC
	forceIndexer := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoB.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := forceIndexer.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 1,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedResult, result)

	require.NoError(t, err)
}

func TestForceIndexRepositoryIncremental(t *testing.T) {
	t.Parallel()

	initialCommit := "5f923865dde3436854e9ceb9cdb7815618d4e849"

	indexDir := t.TempDir()
	initialIndexer := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		TargetSHA:          initialCommit,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	initialResult, err := initialIndexer.IndexRepository(context.Background())

	require.NoError(t, err)
	require.Len(t, getIndexFiles(indexDir, RepoA.ProjectID), 1)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 32,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedResult, initialResult)

	incrementalIndexer := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	incrementalResult, err := incrementalIndexer.IndexRepository(context.Background())

	require.NoError(t, err)
	require.Len(t, getIndexFiles(indexDir, RepoA.ProjectID), 3)

	expectedIncrementalResult := &indexer.IndexingResult{
		ModifiedFilesCount: 7,
		DeletedFilesCount:  1,
	}
	require.Equal(t, expectedIncrementalResult, incrementalResult)

	forceIndexer := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		TargetSHA:          initialCommit,
		ForceReindex:       true,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	result, err := forceIndexer.IndexRepository(context.Background())

	require.NoError(t, err)
	require.Equal(t, expectedResult, result)
	require.Len(t, getIndexFiles(indexDir, RepoA.ProjectID), 1)
}

func TestForceIndexRepositoryWhenInitialIndexing(t *testing.T) {
	t.Parallel()

	indexDir := t.TempDir()
	idxr := indexer.Indexer{
		IndexDir:           indexDir,
		ProjectID:          RepoA.ProjectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: RepoA.Repo,
		ForceReindex:       true,
		Writer:             &indexer.DefaultIndexWriter{},
	}

	incrementalResult, err := idxr.IndexRepository(context.Background())

	require.NoError(t, err)

	expectedResult := &indexer.IndexingResult{
		ModifiedFilesCount: 38,
		DeletedFilesCount:  0,
	}
	require.Equal(t, expectedResult, incrementalResult)
}

func TestTasksWithDeletePayload(t *testing.T) {
	t.Parallel()

	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name": "delete",
				"payload": map[string]interface{}{
					"RepoId": RepoA.ProjectID,
					"Callback": map[string]interface{}{
						"name": "delete",
					},
				},
			},
		},
		"truncate": false,
	}
	bodyJson := bodyJson(body)
	s := defaultIndexServer()
	s.CallbackApi = callback.CallbackApi{
		Client: &http.Client{},
	}
	dir := t.TempDir()
	s.IndexBuilder = server.DefaultIndexBuilder{
		IndexDir: dir,
	}

	otherIndexFilePath := filepath.Join(dir, "31_v16.00000.zoekt")
	targetRepoDeleteFile := filepath.Join(dir, "repos_to_delete", fmt.Sprintf("%v.delete", RepoA.ProjectID))
	uuidFilePath := filepath.Join(dir, "node.uuid")
	_, _ = os.Create(otherIndexFilePath)
	_, _ = os.Create(uuidFilePath)

	require.True(t, isFileExists(otherIndexFilePath))
	require.False(t, isFileExists(targetRepoDeleteFile))
	require.True(t, isFileExists(uuidFilePath))

	var requestedURL url.URL
	var requestedBody interface{}
	var wg sync.WaitGroup
	wg.Add(1)
	svr := stubServer(&requestedURL, &requestedBody, &wg)
	defer svr.Close()
	s.CallbackApi.GitlabURL = svr.URL
	s.CallbackApi.Secret = []byte("secret")
	s.CallbackApi.NodeUUID = nodeUUID
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))

	task_request_response.Process(context.Background(), bodyJson, s)
	wg.Wait()

	require.True(t, isFileExists(otherIndexFilePath))
	require.True(t, isFileExists(targetRepoDeleteFile))
	require.True(t, isFileExists(uuidFilePath))
	require.Equal(t, expectedPath, requestedURL.Path)
}

func TestTasksWithIndexPayload(t *testing.T) {
	t.Parallel()
	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(RepoA),
			},
		},
	}
	bodyJson := bodyJson(body)

	s := defaultIndexServer()
	dir := t.TempDir()
	s.IndexBuilder = server.DefaultIndexBuilder{
		IndexDir: dir,
	}

	var requestedURL url.URL
	var requestedBody interface{}
	var wg sync.WaitGroup
	wg.Add(1)
	svr := stubServer(&requestedURL, &requestedBody, &wg)
	defer svr.Close()
	s.CallbackApi.GitlabURL = svr.URL
	s.CallbackApi.Secret = []byte("secret")
	s.CallbackApi.NodeUUID = nodeUUID
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))

	require.Empty(t, getIndexFiles(dir, RepoA.ProjectID))

	task_request_response.Process(context.Background(), bodyJson, s)
	wg.Wait()

	require.Equal(t, expectedPath, requestedURL.Path)
	require.Len(t, getIndexFiles(dir, RepoA.ProjectID), 1)

	searchResult, err := webserverSearchOnce(dir, webserverPort3, RepoA.ProjectID, "test")

	require.NoError(t, err)
	require.Equal(t, 11, searchResult.Result.FileCount)
	require.Equal(t, 38, searchResult.Result.MatchCount)
}

func TestTasksWithOptimizedPerformancePayload(t *testing.T) {
	t.Parallel()
	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(RepoA),
			},
		},
		"optimized_performance": true,
	}
	bodyJson := bodyJson(body)

	s := defaultIndexServer()
	dir := t.TempDir()
	s.IndexBuilder = server.DefaultIndexBuilder{
		IndexDir: dir,
	}

	var requestedURL url.URL
	var requestedBody interface{}
	var wg sync.WaitGroup
	wg.Add(1)
	svr := stubServer(&requestedURL, &requestedBody, &wg)
	defer svr.Close()
	s.CallbackApi.GitlabURL = svr.URL
	s.CallbackApi.Secret = []byte("secret")
	s.CallbackApi.NodeUUID = nodeUUID
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))

	require.Empty(t, getIndexFiles(dir, RepoA.ProjectID))

	task_request_response.Process(context.Background(), bodyJson, s)
	wg.Wait()

	require.Equal(t, expectedPath, requestedURL.Path)
	require.Len(t, getIndexFiles(dir, RepoA.ProjectID), 1)
	searchResult, err := webserverSearchOnce(dir, webserverPort4, RepoA.ProjectID, "test")
	if err != nil {
		require.NoError(t, err)
	}
	require.Equal(t, 11, searchResult.Result.FileCount)
	require.Equal(t, 38, searchResult.Result.MatchCount)
}

func TestTasksWithIndexPayloadForNonExistingRepo(t *testing.T) {
	t.Parallel()
	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(NonExistingRepo),
			},
		},
	}
	bodyJson := bodyJson(body)

	s := defaultIndexServer()
	dir := t.TempDir()
	s.IndexBuilder = server.DefaultIndexBuilder{
		IndexDir: dir,
	}

	var requestedURL url.URL
	var requestedBody interface{}
	var wg sync.WaitGroup
	wg.Add(1)
	svr := stubServer(&requestedURL, &requestedBody, &wg)
	defer svr.Close()
	s.CallbackApi.GitlabURL = svr.URL
	s.CallbackApi.Secret = []byte("secret")
	s.CallbackApi.NodeUUID = nodeUUID
	expectedPath := fmt.Sprintf("/api/v4/internal/search/zoekt/%s/callback", url.PathEscape(nodeUUID))

	task_request_response.Process(context.Background(), bodyJson, s)
	wg.Wait()

	require.Equal(t, expectedPath, requestedURL.Path)
	require.Equal(t, true, requestedBody.(map[string]interface{})["success"])
	require.Empty(t, getIndexFiles(dir, NonExistingRepo.ProjectID))
}

func TestTasksWithTruncatePayload(t *testing.T) {
	t.Parallel()
	body := map[string]interface{}{
		"tasks": []map[string]interface{}{
			{
				"name":    "index",
				"payload": buildIndexTaskPayload(RepoA),
			},
		},
		"truncate": true,
	}
	bodyJson := bodyJson(body)

	s := defaultIndexServer()
	dir := t.TempDir()
	s.IndexBuilder = server.DefaultIndexBuilder{
		IndexDir: dir,
	}

	indexFilePath := filepath.Join(dir, "31_v16.00000.zoekt")
	uuidFilePath := filepath.Join(dir, "node.uuid")
	_, _ = os.Create(indexFilePath)
	_, _ = os.Create(uuidFilePath)

	require.True(t, isFileExists(indexFilePath))
	require.True(t, isFileExists(uuidFilePath))

	var requestedURL url.URL
	var requestedBody interface{}
	var wg sync.WaitGroup
	wg.Add(1)
	svr := stubServer(&requestedURL, &requestedBody, &wg)
	defer svr.Close()
	s.CallbackApi.GitlabURL = svr.URL
	s.CallbackApi.Secret = []byte("secret")
	s.CallbackApi.NodeUUID = nodeUUID

	task_request_response.Process(context.Background(), bodyJson, s)
	wg.Wait()

	require.False(t, isFileExists(indexFilePath))
	require.True(t, isFileExists(uuidFilePath))
}

func defaultIndexServer() *server.IndexServer {
	return &server.IndexServer{
		PathPrefix:   pathPrefix,
		IndexingLock: indexing_lock.NewIndexingLock(),
		Searcher: search.NewSearcher(&http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}),
		CallbackApi: callback.CallbackApi{
			Client: &http.Client{},
		},
	}
}

func stubServer(svrRequestedPath *url.URL, requestedBody interface{}, wg *sync.WaitGroup) *httptest.Server {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		*svrRequestedPath = *r.URL
		buf, _ := io.ReadAll(r.Body)
		err := json.Unmarshal(buf, &requestedBody)
		if err != nil {
			panic(err)
		}
		wg.Done()
	}))
	return svr
}

func bodyJson(body map[string]interface{}) []byte {
	out, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}
	return out
}

func buildIndexTaskPayload(repo testRepository) map[string]any {
	result := make(map[string]any)
	result["GitalyConnectionInfo"] = map[string]interface{}{
		"Address": gitalyConnInfo.Address,
		"Token":   gitalyConnInfo.Token,
		"Storage": gitalyConnInfo.Storage,
		"Path":    repo.Repo,
	}
	result["Callback"] = map[string]interface{}{
		"name": "index",
		"payload": map[string]interface{}{
			"project_id": repo.ProjectID,
		},
	}
	result["RepoId"] = repo.ProjectID
	result["FileSizeLimit"] = 10000000
	result["Timeout"] = "10s"
	return result
}

func isFileExists(filePath string) bool {
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return false
	}
	return err == nil
}
