package main

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/callback"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/middleware_logger"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/node_uuid"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/profiler"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/search"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/server"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/task_request"
	"go.uber.org/automaxprocs/maxprocs"
	"golang.org/x/sync/errgroup"
)

const (
	serviceName         = "gitlab-zoekt-indexer"
	serverShutdownDelay = 10 * time.Minute
)

var (
	// Overriden in the makefile
	Version   = "dev"
	BuildTime = ""
)

type options struct {
	indexDir       string
	pathPrefix     string
	listen         string
	nodeName       string
	nodeUUID       string
	selfURL        string
	searchURL      string
	gitlabURL      string
	secretFilePath string
}

func parseOptions() (*options, error) {
	versionFlag := flag.Bool("version", false, "Print the version and exit")
	indexDir := flag.String("index_dir", "", "directory holding index shards.")
	pathPrefix := flag.String("path_prefix", "/indexer", "prefix for the routes")
	nodeNameFlag := flag.String("node_name", "", "name of the node")
	selfURL := flag.String("self_url", "", "the URL to reach the node")
	searchURL := flag.String("search_url", "", "the URL to reach the webserver if it differs from self_url")
	gitlabURL := flag.String("gitlab_url", "", "gitlab URL")
	secretFilePath := flag.String("secret_path", "", "gitlab shared secret file path")
	listen := flag.String("listen", ":6060", "listen on this address.")

	flag.Parse()

	if *versionFlag {
		_, _ = fmt.Fprintf(os.Stdout, "%s %s (built at: %s)\n", os.Args[0], Version, BuildTime)
		os.Exit(0)
	}

	if *indexDir == "" {
		err := fmt.Errorf("Usage: %s [ --version | --index_dir=<DIR> | --path_prefix=<PREFIX> | --listen=:<PORT> | --node_name=<NAME> | --self_url=<URL> | --gitlab_url=<URL> ]\nMust set -index_dir\n", os.Args[0])
		return nil, err
	}

	if err := server.CreateIndexDir(*indexDir); err != nil {
		return nil, err
	}

	nodeName := *nodeNameFlag
	if nodeName == "" {
		hostName, err := os.Hostname()
		if err != nil {
			return nil, err
		}
		nodeName = hostName
	}

	n := node_uuid.NewNodeUUID(*indexDir)
	nodeUUID, err := n.Get()

	if err != nil {
		return nil, err
	}

	if *searchURL == "" {
		*searchURL = *selfURL
	}

	return &options{
		indexDir:       *indexDir,
		pathPrefix:     *pathPrefix,
		listen:         *listen,
		nodeName:       nodeName,
		nodeUUID:       nodeUUID,
		selfURL:        *selfURL,
		searchURL:      *searchURL,
		gitlabURL:      *gitlabURL,
		secretFilePath: *secretFilePath,
	}, nil
}

func run() error {
	middleware_logger.SetUpLogger()
	opts, err := parseOptions()

	if err != nil {
		return err
	}

	var callbackApiInstance callback.CallbackApi

	// Tune GOMAXPROCS to match Linux container CPU quota.
	_, _ = maxprocs.Set()

	indexingLock := indexing_lock.NewIndexingLock()

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)

	s := &server.IndexServer{
		PathPrefix: opts.pathPrefix,
		IndexBuilder: server.DefaultIndexBuilder{
			IndexDir: opts.indexDir,
		},
		IndexingLock: indexingLock,
		Searcher: search.NewSearcher(&http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}),
	}

	if opts.gitlabURL != "" && opts.selfURL != "" {
		callbackApiInstance, err = callback.NewCallbackApi(opts.gitlabURL, opts.nodeUUID, opts.secretFilePath, &http.Client{})

		if err != nil {
			return err
		}

		s.CallbackApi = callbackApiInstance

		concurrency := runtime.GOMAXPROCS(0)
		slog.Info("starting taskRequest", "node_name", opts.nodeName, "index_url", opts.selfURL, "search_url", opts.selfURL, "gitlab_url", opts.gitlabURL, "concurrency", concurrency)
		taskRequest, err := task_request.NewTaskRequestTimer(&task_request.NewTaskRequestTimerParams{
			IndexDir:       opts.indexDir,
			NodeName:       opts.nodeName,
			NodeUUID:       opts.nodeUUID,
			Version:        Version,
			SelfURL:        opts.selfURL,
			SearchURL:      opts.searchURL,
			GitlabURL:      opts.gitlabURL,
			SecretFilePath: opts.secretFilePath,
			Concurrency:    concurrency,
			IndexingLock:   indexingLock,
		})
		if err != nil {
			return err
		}

		g.Go(func() error {
			return taskRequest.Start(ctx, s)
		})
	}

	p := profiler.NewProfiler()
	p.Init(serviceName, Version)

	httpServer := &http.Server{ //nolint:gosec
		Addr:    opts.listen,
		Handler: s.Router(),
	}

	g.Go(func() error {
		if err := s.StartIndexingApi(httpServer); err != nil && err != http.ErrServerClosed { //nolint:errorlint
			return fmt.Errorf("Failed to start indexing API: %w", err)
		}

		return nil
	})

	g.Go(func() error {
		return s.StartFileCleaner(ctx)
	})

	<-ctx.Done()

	slog.Info("gracefully shutting down...")

	g.Go(func() error {
		timeoutCtx, timeoutCancel := context.WithTimeout(context.Background(), serverShutdownDelay)
		defer timeoutCancel()

		return httpServer.Shutdown(timeoutCtx)
	})

	return g.Wait()
}

func main() {
	err := run()

	if err != nil {
		slog.Error("program aborted", "error", err)
		os.Exit(1)
	}
}
