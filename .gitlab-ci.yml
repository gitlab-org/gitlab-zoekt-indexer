workflow:
  rules: &workflow_rules
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `main` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - component: ${CI_SERVER_FQDN}/gitlab-org/components/danger-review/danger-review@2.0.0

.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
  before_script:
    - mkdir -p .go
  cache:
    key:
      files:
        - .gitlab-ci.yml
        - go.sum
    paths:
      - .go/pkg/mod/

.test-default:
  extends: .go-cache
  stage: test
  services:
    - name: registry.gitlab.com/gitlab-org/build/cng/gitaly:17-7-stable
      # Disable the hooks so we don't have to stub the GitLab API
      command: ["bash", "-c", "mkdir -p /home/git/repositories && rm -rf /srv/gitlab-shell/hooks/* && echo -e \"\n[auth]\ntoken = 'secret_token'\n\" >> /etc/gitaly/config.toml && exec /usr/bin/env GITALY_TESTING_NO_GIT_HOOKS=1 /scripts/process-wrapper"]
      alias: gitaly
  before_script:
    - go install github.com/sourcegraph/zoekt/cmd/zoekt-webserver@f75df3d8a3f8de3003da19567bf13429563036cb
    - export PATH="$GOPATH/bin:$PATH"
  script:
    - go test -race -v ./... -cover -coverprofile=test.coverage -covermode atomic
  after_script:
    - go install github.com/boumenot/gocover-cobertura@latest
    - $GOPATH/bin/gocover-cobertura < test.coverage > coverage.xml
  variables:
    GITALY_CONNECTION_INFO: '{"address":"tcp://gitaly:8075", "storage":"default", "token": "secret_token"}'
    INTEGRATION: 'true'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

.lint-default:
  stage: test
  image: golang:1.23

semgrep-sast:
  rules: *workflow_rules

gemnasium-dependency_scanning:
  rules: *workflow_rules

secret_detection:
  rules: *workflow_rules

danger-review:
  before_script:
    - bundle init
    - bundle add gitlab-dangerfiles

lint:golangci:
  extends: .lint-default
  image: golangci/golangci-lint:v1.64.6
  script:
    - golangci-lint run -v

lint:shadow:
  extends: .lint-default
  script:
    - go install golang.org/x/tools/go/analysis/passes/shadow/cmd/shadow@latest
    - shadow ./...

lint:go-mod:
  extends: .lint-default
  script:
    - go mod tidy
    - |
      STATUS=$( git status --porcelain go.mod go.sum )
      if [ ! -z "$STATUS" ]; then
        git diff go.mod go.sum
        echo ""
        echo "Running go mod tidy modified go.mod and/or go.sum"
        exit 1
      fi

tests:
  extends: .test-default
  image: golang:${GO_VERSION}
  parallel:
    matrix:
      - GO_VERSION: ["1.23"]

code_navigation:
  image: sourcegraph/scip-go:v0.1.23
  allow_failure: true
  script:
    - go mod download && scip-go
    - |
      env \
      TAG="v0.4.0" \
      OS="$(uname -s | tr '[:upper:]' '[:lower:]')" \
      ARCH="$(uname -m | sed -e 's/x86_64/amd64/')" \
      bash -c 'curl --location "https://github.com/sourcegraph/scip/releases/download/$TAG/scip-$OS-$ARCH.tar.gz"' \
      | tar xzf - scip
    - chmod +x scip
    - ./scip convert --from index.scip --to dump.lsif
  artifacts:
    reports:
      lsif: dump.lsif
